#Description: This script generates Density of States 
#from VASP calculations 
#---------------------------------------------------
# Required Files: vasprun.xml, CONTCAR, POTCAR, OUTCAR
# Required Packages: Pymatgen version 2017.06.08, matplotlib version (>=) 2.0.2 
# Usage: python density_of_states.py 
#
# Output: 
#         Prints d-band center
#         summary_atomic.csv: Data for projected DOS 
#                  for each atom
#         dos.png: A quick density_of_states plot if specified (see last line of this file) 
#         summary_orbital.csv: Data for orbital (s,p,d)
#                  projected DOS for each atom if specified (see line 173) 
#         NOTE: order of atomic/orbital DOS printed in *.csv
#                  file is the same as that in your CONTCAR 
#         NOTE: This program works only for ISPIN=2 calculation for now 
#---------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   Jan 23, 2018 
#---------------------------------------------------

import csv
import pymatgen as mg
import numpy as np
from pymatgen.io.vasp.outputs import Vasprun
from pymatgen.electronic_structure.core import Spin, Orbital
from pymatgen.electronic_structure.dos import Dos
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import os

def make_raw_data():
    energy_occup = []

    with open("OUTCAR") as f:
        lines = f.readlines()
        for n, line in enumerate(lines):
            if "E-fermi" in line:
                Efermi = float(lines[n].split()[2])
            if "ISPIN" in line:
                ispin = int(lines[n].split()[2])
            if "NBANDS" in line:
                nbands = int(lines[n].split()[-1])
                nkpts = int(lines[n].split()[3])
            if "band No.  band energies     occupation" in line:
                energy_occup.append(lines[n+1:n+nbands+1])

    #print "ISPIN detected: %d" %ispin
    #print "Number of k-points detected: %d" %nkpts
    #print "Number of bands (NBANDS) detected: %d" %nbands
    fout = open("data","a")
    for kpoints in range(len(energy_occup)):
         for nband in range(nbands):
             item = energy_occup[kpoints][nband].split()
             print >> fout, "%4d    % 3.4f    %3.4f" %(int(item[0]), float(item[1]), float(item[2]))

    return Efermi, ispin, nkpts, nbands

def get_homo_lumo():
    Efermi, ispin, nkpts, nbands = make_raw_data()
    f2 = open("data","r")
    occupied_energies, unoccupied_energies = [], []
    for kp in range(ispin*nkpts):
        for nband in range(nbands):
            item = f2.readline().split()
            band_energy = float(item[1])
            occupation = float(item[2])
            if int(occupation) == 1 or int(occupation) == 2:
                occupied_energies.append(band_energy)
            if int(occupation) == 0:
                unoccupied_energies.append(band_energy)
            else:
                "Print fractional occupations not implemented"
    homo = max(occupied_energies)
    lumo = min(unoccupied_energies)
    return Efermi, homo, lumo

def get_dos(smear, labels, calc_dband=False):
    atom_header = []; orb_header = []; d_band_array = np.zeros(nedos) 
    orb_array = np.arange(1,nedos+1).reshape((-1,1))
    orb_arrayup = np.arange(1,nedos+1).reshape((-1,1))
    orb_arraydown = np.arange(1,nedos+1).reshape((-1,1))
    atom_array = np.arange(1,nedos+1).reshape((-1,1))
    element_counter = 0
    dos_list = []
    d_elements_raw = raw_input("Enter element index for which you want d-band center\nExample: \
If your POSCAR has Ti O Cu, enter '0 2' (without quotes) to get d-band center of Ti+Cu: ")
    d_elements = [int(de) for de in d_elements_raw.split()]

    for i in range(len(natoms_list)): 
        dosatom = np.zeros(nedos)
        for i_elem in range(natoms_list[i]): 
            dos = dosrun.pdos[element_counter] 
            keys = dos.keys()
            #Up Spin
            dictsup = {'S': dos[keys[0]][Spin.up]}
            dictpup = {'P': dos[keys[1]][Spin.up]}
            dictdup = {'D': dos[keys[2]][Spin.up]}

            #Orbital resolved DOS            
            orbSmearedsup = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictsup).get_smeared_densities(smear)
            orbSmearedpup = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictpup).get_smeared_densities(smear)
            orbSmeareddup = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictdup).get_smeared_densities(smear)
            #Matrix to store all orbital resolved DOS
            orb_arrayup = np.append(orb_arrayup, orbSmearedsup.values()[0].reshape((-1,1)), axis=1)
            orb_arrayup = np.append(orb_arrayup, orbSmearedpup.values()[0].reshape((-1,1)), axis=1)
            orb_arrayup = np.append(orb_arrayup, orbSmeareddup.values()[0].reshape((-1,1)), axis=1)
            
            #Down Spin
            dictsdown = {'S': dos[keys[0]][Spin.down]}
            dictpdown = {'P': dos[keys[1]][Spin.down]}
            dictddown = {'D': dos[keys[2]][Spin.down]}

            #Orbital resolved DOS            
            orbSmearedsdown = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictsdown).get_smeared_densities(smear)
            orbSmearedpdown = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictpdown).get_smeared_densities(smear)
            orbSmearedddown = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictddown).get_smeared_densities(smear)
            #Matrix to store all orbital resolved DOS
            orb_arraydown = np.append(orb_arraydown, orbSmearedsdown.values()[0].reshape((-1,1)), axis=1)
            orb_arraydown = np.append(orb_arraydown, orbSmearedpdown.values()[0].reshape((-1,1)), axis=1)
            orb_arraydown = np.append(orb_arraydown, orbSmearedddown.values()[0].reshape((-1,1)), axis=1)
            
           #D-band center_related 
            if calc_dband == True:
                if i in d_elements: 
                    d_band_array = d_band_array + orbSmeareddup.values()[0] + orbSmearedddown.values()[0]
           #End D-band center_related 
  
            #Distinct atomic species resolved DOS            
            dos_add = [ii + jj + kk + ll + mm + nn for ii, jj, kk, ll, mm, nn in zip(dictsup.values(), dictpup.values(), dictdup.values(), dictsdown.values(), dictpdown.values(), dictddown.values())][0]
            #Adding all dos per atomic species 
            dosatom += dos_add  
            dictatom = {'Atom': dosatom} 
            #AtomSmeared stores DOS based of each distinct species. DOS of distinct species involves addition
            #of DOS for each similar kind of atom 
            AtomSmeared = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictatom).get_smeared_densities(smear)
    
            #Atom resolved DOS. #dict_each_atom is to get DOS for each atom (not distinct species)           
            dictatom_each_atom = {'Atom': dos_add} 
            AtomSmeared_each_atom = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dictatom_each_atom).get_smeared_densities(smear)
            #Matrix to store all atomic DOS
            atom_array = np.append(atom_array, AtomSmeared_each_atom.values()[0].reshape((-1,1)), axis=1) 

            element_counter += 1
        #List to store DOS per distinct atomic species for automatic plotting
        dos_list.append(AtomSmeared.values()[0])

    #Finally add in Total DOS and E-Efermi energies to orb_array and atom_array matrices
    totup = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dosrun.tdos.densities).get_smeared_densities(smear)[Spin.up]
    totdown = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dosrun.tdos.densities).get_smeared_densities(smear)[Spin.down]
    totdos = totup + totdown

    e_ef = (dosrun.tdos.energies-dosrun.tdos.efermi).reshape((-1,1))
    orb_array = np.append(orb_array, orb_arrayup+orb_arraydown, axis=1)
    orb_array = np.append(orb_array, totdos.reshape((-1,1)), axis=1)
    orb_array = np.append(orb_array, e_ef, axis=1)
    atom_array = np.append(atom_array, totdos.reshape((-1,1)), axis=1)
    atom_array = np.append(atom_array, e_ef, axis=1)

    #Build header for atomic DOS and orbital DOS
    for i, j in zip(natoms_list, labels):
        count = 0
        while count < i:
            atom_header.append(j)
            orb_header.append('%s-s'%j)
            orb_header.append('%s-p'%j)
            orb_header.append('%s-d'%j)
            count += 1

    atom_header = ', '.join(['NEDOS'] + atom_header + ['Total'] + ['E-Efermi (eV)'])
    orb_header = ', '.join(['NEDOS'] + orb_header + ['Total'] + ['E-Efermi (eV)'])
    
    #Write matrix to file
    #np.savetxt('summary_orbital.csv', orb_array, header=orb_header, fmt='%1.7e', delimiter=',')
    np.savetxt('summary_atomic.csv', atom_array, header=atom_header, fmt='%1.7e', delimiter=',')
    
    #d band center related
    if calc_dband == True:
        e_ef_row = dosrun.tdos.energies-dosrun.tdos.efermi 
        input_class = int(raw_input("Enter 0 for metal and 1 for semiconductor: "))
        idx = []; e_ef_list = []; dband_list = []
        if input_class == 0:
    #        emin = float(raw_input("enter Energy range Emin (-10eV):"))
    #        emax = float(raw_input("enter Energy range Emax (5eV):"))
            print "Using -10, 5eV range for d-band center calculation" 
            emin, emax = -10, 5
            for index, ee in enumerate(e_ef_row):
                 if ee > emin and ee < emax:
                     idx.append(index)
            for ii in idx:
                e_ef_list.append(e_ef_row[ii]) 
                dband_list.append(d_band_array[ii]) 
            #plt.plot(e_ef_list, dband_list)
            #plt.savefig('test.png')
        if input_class == 1:
            print "Using -10, 0eV range for d-band center calculation" 
            emin, emax = -10, 0 
            for index, ee in enumerate(e_ef_row):
                 if ee > emin and ee < emax:     
                     e_ef_list.append(ee) 
                     dband_list.append(d_band_array[index]) 

        with open("data", "w") as f:
            for i, j in zip(e_ef_list, dband_list):
                print >> f, i, j
 
        I = np.trapz(np.array(dband_list), np.array(e_ef_list))
        dBandCenter = np.trapz(np.array(dband_list) * np.array(e_ef_list), np.array(e_ef_list)) / I
        print "d band center: %.2f (eV)" %dBandCenter 
    #End d band center related

    #return dos_list for automatic plotting 
    return dos_list

def get_total_dos(smear):
    totup = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dosrun.tdos.densities).get_smeared_densities(smear)[Spin.up]
    totdown = Dos(dosrun.tdos.efermi, dosrun.tdos.energies, dosrun.tdos.densities).get_smeared_densities(smear)[Spin.down]
    totdos = totup + totdown

    return totdos

def format():
        for axis in ['top','bottom','left','right']:
                ax.spines[axis].set_linewidth(1)

        for tick in ax.yaxis.get_ticklabels():
                tick.set_fontsize(30)
                tick.set_weight('bold')

        for tick in ax.xaxis.get_ticklabels():
                tick.set_fontsize(30)
                tick.set_weight('bold')

        ax.tick_params(axis='x', which='both', direction='in', length=10, width=1, pad=8, top='off')
        ax.tick_params(axis='y', which='major', direction='in', length=10, width=1, pad=8, right='off', left='off')

def plot_dos(labels):    
    print "Generating parsed DOS and a quick DOS plot\n" 
    fig = plt.figure(figsize=(9.6,6), dpi=300)
    global ax
    ax = plt.subplot2grid((1,1), (0,0)) 
    e_ef = (dosrun.tdos.energies-dosrun.tdos.efermi)

    colors = ['red', 'k', 'blue']
    for pp, label, color in zip(get_dos(smear, labels), labels, colors): 
        ax.plot(e_ef, pp,linewidth=3, color=color, label=label) 
    
    ax.set_ylabel('Density of States\n(Arbitrary Units)',fontsize=26,fontweight='bold')
    ax.set_xlabel('Energy (eV)',fontsize=26,fontweight='bold')
    ax.set_yticklabels([])
    plt.axis([-8,4,0,20])
    format()
    font= FontProperties(weight='bold',size=24)
    plt.legend(loc='upper right', bbox_to_anchor=(0.95,0.95), prop=font, edgecolor='k')
    fig.set_tight_layout(True)
    plt.savefig("dos_cbm.png", format="png")

if __name__ == '__main__':
    with open("CONTCAR") as contcar:
        element_list = contcar.readline()
    element_list = raw_input("Enter list of elements separated by space \n \
in the same order as in POSCAR.\nExample: 'Bi S O' (without quoutes): ") 
    labels = element_list.split()  
    smear = 0.08

    dosrun = Vasprun("./vasprun.xml")
    nedos = len(dosrun.tdos.energies) 
    #Virtually setting efermi as CBM 
    efermi, homo, lumo = get_homo_lumo()
    dosrun.tdos.efermi = lumo - 0.2

    with open("CONTCAR") as f:
        [f.readline() for _ in range(6)]
        line = f.readline() 
        natoms_list = [int(aa) for aa in line.split()]
    
    get_dos(smear, labels, calc_dband=True)
    #plot_dos(labels)
    os.system('rm data') 