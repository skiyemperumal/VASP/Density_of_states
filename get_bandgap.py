#Description: This script calculates band gap from 
#eigenvalues printed in OUTCAR from VASP Calculation
#---------------------------------------------------
# Required Files: OUTCAR
# Usage: python get_bandgap.py 
#
# Output: 
#         Prints out band gap on screen 
#---------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   Jan 17, 2018 
#---------------------------------------------------

import os

def make_raw_data():
    energy_occup = []

    with open("OUTCAR") as f:
	lines = f.readlines()
	for n, line in enumerate(lines):
	    if "E-fermi" in line: 
		Efermi = float(lines[n].split()[2])
	    if "ISPIN" in line: 
		ispin = int(lines[n].split()[2])
	    if "NBANDS=" in line: 
		nbands = int(lines[n].split()[-1]) 
		nkpts = int(lines[n].split()[3]) 
	    if "band No.  band energies     occupation" in line:
		energy_occup.append(lines[n+1:n+nbands+1])

    print "ISPIN detected: %d" %ispin
    print "Number of k-points detected: %d" %nkpts
    print "Number of bands (NBANDS) detected: %d" %nbands 
    fout = open("data","a")
    for kpoints in range(len(energy_occup)):
	 for nband in range(nbands):
	     item = energy_occup[kpoints][nband].split()
	     print >> fout, "%4d    % 3.4f    %3.4f" %(int(item[0]), float(item[1]), float(item[2]))

    return Efermi, ispin, nkpts, nbands

def get_homo_lumo():
    Efermi, ispin, nkpts, nbands = make_raw_data()
    f2 = open("data","r") 
    occupied_energies, unoccupied_energies = [], []
    for kp in range(ispin*nkpts):
	for nband in range(nbands):
	    item = f2.readline().split()
	    band_energy = float(item[1])
	    occupation = float(item[2])
	    if int(occupation) == 1 or int(occupation) == 2:
		occupied_energies.append(band_energy)
	    if int(occupation) == 0: 
		unoccupied_energies.append(band_energy)
	    else: 
		"Print fractional occupations not implemented" 
    homo = max(occupied_energies)
    lumo = min(unoccupied_energies)
    return Efermi, homo, lumo 

if __name__ == "__main__":    
    Efermi, homo, lumo = get_homo_lumo()

    print "E-fermi is: %.4f eV"%Efermi
    print "CBM/HOMO is: %.4f eV"%homo 
    print "VBM/LUMO is: %.4f eV"%lumo 
    print "Band gap is: %.4f eV"%(lumo - homo)
    os.system('rm data')

